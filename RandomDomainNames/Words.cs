﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomDomainNames
{
	public class Words
	{
		IWords words;		// Ioc determines how to read in random words
		List<String> randomWords;

		public Words(IWords words)
		{
			this.words = words;
		}

		public string GenerateName(int howManyWords)
		{
			randomWords = words.ReadWords(howManyWords);

			string name = String.Join("-", randomWords);
			string tld  = words.ReadTLD();

			string fullDomainName = name + "." + tld;

			return fullDomainName;
		}
	}
}
