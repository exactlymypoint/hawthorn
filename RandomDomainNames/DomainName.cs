﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomDomainNames
{
	public class DomainName
	{
		IWords _words;

		public DomainName(IWords words)
		{
			_words = words;
		}

		public string GetRandomDomainName(int howMany)
		{
			Words words = new Words(_words);

			string name = words.GenerateName(howMany);

			return name;
		}
	}
}
