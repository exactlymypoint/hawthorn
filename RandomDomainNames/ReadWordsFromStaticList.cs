﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomDomainNames
{
	public class ReadWordsFromStaticList : IWords
	{
		static Random rnd = new Random();

		public List<string> ReadWords(int howMany)
		{
			List<int> randomIndex = RandomNumbers.GetRandomNumbers(howMany, words.Count());

			List<string> randomWords = new List<string>();

			foreach (int item in randomIndex)
			{
				randomWords.Add(words[item]);
			}

			return randomWords;
		}

		public string ReadTLD()
		{
			int randomNumber = RandomNumbers.rnd.Next(0, tld.Count() - 1);

			return tld[randomNumber];
		}

		List<string> tld = new List<string>
		{
			"com",
			"net",
			"edu",
			"org"
		};

		List<string> words = new List<string>
		{
			"entry",
			"property",
			"analyst",
			"system",
			"reception",
			"poem",
			"education",
			"football",
			"hospital",
			"series",
			"combination",
			"debt",
			"insect",
			"investment",
			"difficulty",
			"thing",
			"thanks",
			"extent",
			"assumption",
			"knowledge",
			"initiative",
			"cell",
			"poetry",
			"finding",
			"story",
			"county",
			"concept",
			"lake",
			"historian",
			"drawer",
			"engineering",
			"unit",
			"driver",
			"stranger",
			"fortune",
			"restaurant",
			"garbage",
			"product",
			"tooth",
			"leadership",
			"indication",
			"distribution",
			"percentage",
			"department",
			"setting",
			"wealth",
			"excitement",
			"language",
			"camera",
			"winner"
		};
	}
}
