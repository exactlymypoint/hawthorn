﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomDomainNames
{
	public static class RandomNumbers
	{
		public static Random rnd = new Random();


		public static List<int> GetRandomNumbers(int howMany, int maxValue)
		{
			List<int> randomNumbers = new List<int>();

			while (randomNumbers.Count < howMany)
			{
				int r = rnd.Next(0, maxValue - 1);
				bool alreadyExists = randomNumbers.Contains(r);
				if (!alreadyExists)
					randomNumbers.Add(r);
			}

			return randomNumbers;
		}
	}
}
