﻿using System;
using System.Collections.Generic;

namespace RandomDomainNames
{
	public interface IWords
	{
		List<String> ReadWords(int howMany);
		string ReadTLD();
	}
}