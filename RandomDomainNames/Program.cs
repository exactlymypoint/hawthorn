﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomDomainNames
{
	class Program
	{
		static void Main(string[] args)
		{
			Pgm program = new Pgm();
			program.Run();
		}
	}

	public class Pgm
	{
		public void Run()
		{
			bool validNumber = false;
			int howMany;
			string input;

			// read how many names to create.
			do
			{
				Console.Write("How many random domain names do you want? ");
				input = Console.ReadLine();

				validNumber = int.TryParse(input, out howMany);

			// only continue if have a valid number
			} while (!validNumber);

			// the capability exists to inject the type of class to read random
			// words from.  I did not include IoC code because that is time
			// consuming to do, but if I had, it would be useful here
			DomainName domainName = new DomainName(new ReadWordsFromStaticList());
			Console.WriteLine();


			for (int i = 0; i < howMany; i++)
			{
				// will get a minimum of two and max of 7....easily modifiable
				string randomDomainName = domainName.GetRandomDomainName(RandomNumbers.rnd.Next(2, 7));
				Console.WriteLine(randomDomainName);
			}

			Console.WriteLine();
			Console.Write("Press return to end ");
			input = Console.ReadLine();

		}
	}
}
